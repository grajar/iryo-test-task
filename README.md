This project was created to demonstrate the front-end skills of a particular developer. It's a file storage application's front-end built in ReactJS. It comunicates with a given API created and hosted by 3fs.

## Instalation and running

### Setup

Clone the project using:

`git clone https://grajar@bitbucket.org/grajar/iryo-test-task.git`

Before the app can use the 3fs' API, you must enter an API token into the `.env` file located in the root of the project:

`REACT_APP_IRYO_API_TOKEN="[YOUR_IRYO_API_TOKEN]"`

### Development

To run the app on `localhost:3000` use:

`yarn`  
`yarn start`

or

`npm install`  
`npm start`

You can also spin it up in a container using `docker-compose`. It will run on `localhost:3001`.

`docker-compose up -d --build`

### Production

To spin up a production container use `docker-compose-prod.yaml`. It will create an nginx server and serve the app on `localhost:80`.

`docker-compose -f docker-compose-prod.yaml up -d --build`
