import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import "styles/bootstrap/bootstrap.scss";
import "./App.scss";

import appRoutes from "routes";

import { Notifications, Prompt } from "components";
import BucketList from "views/BucketList/BucketList";
import SingleBucket from "views/SingleBucket/SingleBucket";
import NotFound from "views/NotFound";

const App = () => {
  return (
    <div className="app">
      <header className="app-header">
        <Link className="app-header-link" to="/">
          Secure Cloud Storage
        </Link>
      </header>
      <section className="app-content">
        <Switch>
          <Route exact path={appRoutes.BUCKET + "/:bucketId?"} component={SingleBucket} />;
          <Route exact path={appRoutes.ROOT} component={BucketList} />;
          <Route component={NotFound} />;
        </Switch>
      </section>
      <Notifications />
      <Prompt />
    </div>
  );
};

export default App;
