import React from "react";
import { cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { renderBasic, resetStore, history } from "tests/testUtils";
import App from "App.jsx";
import api from "axios";
import endpoints from "api/iryo/urls";

import { mockGet, mockPost } from "tests/__mocks__/iryoMocks";

jest.mock("axios");

afterEach(() => {
  api.get.mockClear();
  api.post.mockClear();
  api.delete.mockClear();
  history.push("/");
  resetStore();
  cleanup();
});

it("renders without crashing", async () => {
  renderBasic(<App />);
});

it("renders 'bucket list'", async () => {
  const { getByTestId, findByTestId } = renderBasic(<App />);
  expect(await findByTestId("global-loader")).not.toBeInTheDocument();
  expect(getByTestId("bucket-list")).toBeInTheDocument();
});

it("creates and displays a new bucket", async () => {
  const { findByText, getByRole, getAllByRole, findByTestId, getByTestId } = renderBasic(<App />);
  expect(await findByTestId("global-loader")).not.toBeInTheDocument();

  // open create bucket form
  let openCreateButton = await findByTestId("open-create-bucket-button");
  expect(openCreateButton).toBeInTheDocument();
  fireEvent.click(openCreateButton);

  // enter a new bucket name
  const nameInput = getByRole("textbox");
  fireEvent.change(nameInput, { target: { value: mockPost.bucket.name } });
  expect(nameInput.value).toEqual(mockPost.bucket.name);

  // select location
  const locationInput = getByRole("listbox");
  fireEvent.change(locationInput, {
    target: { value: mockPost.bucket.location.id, label: mockPost.bucket.location.name },
  });
  expect(locationInput.value).toEqual(mockPost.bucket.location.id);

  // submit new bucket
  const submitButton = getByTestId("create-bucket-submit-button"); // submit button
  expect(submitButton).toBeInTheDocument();
  expect(api.post).toHaveBeenCalledTimes(0);
  const bucketLength = getAllByRole("row").length;
  fireEvent.click(submitButton);

  // check if created
  expect(api.post).toHaveBeenCalledTimes(1);
  expect(api.post).toHaveBeenCalledWith(endpoints.BUCKETS, {
    name: mockPost.bucket.name,
    location: mockPost.bucket.location.id,
  });
  expect(await findByText("Successfully", { exact: false })).toBeInTheDocument(); // new bucket must
  expect(await findByText(mockPost.bucket.name)).toBeInTheDocument(); // new bucket must appear
  expect(getAllByRole("row").length).toEqual(bucketLength + 1); // have 1 bucket more in table than before
  expect(submitButton).not.toBeInTheDocument(); // close create bucket form
});

it("navigates to SingleBucket and displays bucket data", async () => {
  const { getAllByTestId, findByTestId, findByText } = renderBasic(<App />, {});
  expect(await findByTestId("global-loader")).not.toBeInTheDocument();

  let singleBucketLinks = getAllByTestId("single-bucket-link");
  expect(singleBucketLinks[0]).toBeInTheDocument();
  expect(singleBucketLinks[0].getAttribute("href")).toEqual("/bucket/" + mockGet.buckets[0].id);

  fireEvent.click(singleBucketLinks[0]);
  expect(await findByTestId("single-bucket")).toBeInTheDocument();
  expect(await findByText(mockGet.buckets[0].name, { exact: false })).toBeInTheDocument();
});

it("uploads and displays a new object", async () => {
  const { getAllByTestId, findByTestId, findByText, getAllByRole } = renderBasic(<App />);
  expect(await findByTestId("global-loader")).not.toBeInTheDocument();
  let singleBucketLinks = getAllByTestId("single-bucket-link");
  fireEvent.click(singleBucketLinks[0]);
  expect(api.post).toHaveBeenCalledTimes(0);
  expect(await findByTestId("single-bucket")).toBeInTheDocument();

  // find upload and upload file
  const uploadInput = await findByTestId("upload-file-input");
  expect(uploadInput).toBeInTheDocument();
  const initialObjectRows = getAllByRole("row").length;
  const file = new File(["(⌐□_□)"], "chucknorris.png", { type: "image/png" });
  fireEvent.change(uploadInput, {
    target: {
      files: [file],
    },
  });

  expect(api.post).toHaveBeenCalledTimes(1);
  expect(api.post).toBeCalledWith(
    `${endpoints.BUCKETS}/${mockGet.buckets[0].id}${endpoints.OBJECTS}`,
    expect.anything()
  );
  expect(await findByText("chucknorris.png")).toBeInTheDocument();
  expect(getAllByRole("row").length).toEqual(initialObjectRows + 1); // have 1 object more in table than before
});
