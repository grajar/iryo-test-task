import axios from "axios";
import endpoints from "./urls";
import {
  setBuckets,
  setLocations,
  addBucket,
  removeBucket,
  setObjects,
  addObject,
  removeObject,
} from "redux/actions";

const iryoApi = axios.create({
  baseURL: process.env.REACT_APP_IRYO_API_URL,
  headers: { Authorization: `Token ${process.env.REACT_APP_IRYO_API_TOKEN}` },
});

async function listLocations() {
  let { data } = await iryoApi.get(endpoints.LOCATIONS);
  if (data.locations && Array.isArray(data.locations)) {
    let locations = parseDocumentsForRedux(data.locations);
    setLocations(locations);
    return locations;
  }
}

async function listBuckets() {
  let { data } = await iryoApi.get(endpoints.BUCKETS);
  if (data.buckets && Array.isArray(data.buckets)) {
    let buckets = parseDocumentsForRedux(data.buckets);
    setBuckets(buckets);
    return buckets;
  }
  // TODO error handling
}

async function createBucket(name, location) {
  let { data } = await iryoApi.post(endpoints.BUCKETS, {
    name,
    location,
  });
  if (data.bucket) {
    addBucket(data.bucket);
    return data.bucket;
  }
}

async function deleteBucket(bucketId) {
  await iryoApi.delete(`${endpoints.BUCKETS}/${bucketId}`);
  removeBucket(bucketId);
  return true;
}

async function listObjects(bucketId) {
  let { data } = await iryoApi.get(`${endpoints.BUCKETS}${"/" + bucketId}${endpoints.OBJECTS}`);
  if (data.objects) {
    setObjects(bucketId, data.objects);
    return data.objects;
  }
}

async function deleteObject(bucketId, objectName) {
  await iryoApi.delete(`${endpoints.BUCKETS}/${bucketId}${endpoints.OBJECTS}/${objectName}`);
  removeObject(bucketId, objectName);
  return true;
}

async function createObject(bucketId, file) {
  const formData = new FormData();
  formData.append("file", file, file.name);
  const uploadUrl = `${endpoints.BUCKETS}${"/" + bucketId}${endpoints.OBJECTS}`;
  let { data } = await iryoApi.post(uploadUrl, formData);
  if (data && data.name) {
    addObject(bucketId, data);
    return data;
  }
}

function parseDocumentsForRedux(documents) {
  if (Array.isArray(documents)) {
    let parsedDocuments = {};
    documents.forEach(doc => {
      parsedDocuments[doc.id] = doc;
    });
    return parsedDocuments;
  }
  return {};
}

export {
  listLocations,
  listBuckets,
  createBucket,
  deleteBucket,
  listObjects,
  createObject,
  deleteObject,
};
