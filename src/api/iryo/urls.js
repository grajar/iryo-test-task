const endpoints = {
  LOCATIONS: `/locations`,
  BUCKETS: `/buckets`,
  OBJECTS: `/objects`,
};

export default endpoints;
