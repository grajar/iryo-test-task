import React from "react";
import PropTypes from "prop-types";

const Button = props => (
  <button
    id={props.id}
    {...props.inputProps}
    type={props.type}
    onClick={props.onClick}
    className={`btn ${props.className || ""}`}
  >
    {props.children}
  </button>
);

Button.propTypes = {
  inputProps: PropTypes.object,
  id: PropTypes.string,
  onClick: PropTypes.func,
  className: PropTypes.string,
  type: PropTypes.string,
};

export default Button;
