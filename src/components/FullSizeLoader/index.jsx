import React from "react";
import "./index.scss";

const FullSizeLoader = props => (
  <div className="fullSizeLoader" {...props} data-testid="global-loader">
    <div className="box">
      <div className="loader9" />
    </div>
  </div>
);

export default FullSizeLoader;
