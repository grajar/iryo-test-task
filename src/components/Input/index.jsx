import React from "react";
import PropTypes from "prop-types";

const Input = props => (
  <div className="form-group">
    {props.label && <label>{props.label}</label>}
    <input
      {...props.inputProps}
      type="text"
      className={`form-control ${props.className || ""}`}
      onChange={e => props.onChange && props.onChange(e.target.value)}
    />
  </div>
);

Input.propTypes = {
  onChange: PropTypes.func,
  label: PropTypes.string,
  className: PropTypes.string,
  inputProps: PropTypes.object,
};

export default Input;
