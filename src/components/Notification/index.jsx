import React, { Component } from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck,
  faExclamationTriangle,
  faExclamationCircle,
  faInfoCircle,
} from "@fortawesome/free-solid-svg-icons";
import "./index.scss";

const mapTypeToIcon = {
  success: faCheck,
  danger: faExclamationTriangle,
  warning: faExclamationCircle,
  info: faInfoCircle,
};

class Notification extends Component {
  state = {
    icon: faExclamationCircle,
    colorType: "warning",
    message: "",
  };
  componentDidMount() {
    let icon = this.props.icon
      ? this.props.icon
      : this.props.colorType
      ? mapTypeToIcon[this.props.colorType]
      : faExclamationCircle;
    this.setState({
      icon,
      message: this.props.message || "No message",
      colorType: this.props.colorType || "warning",
    });
  }
  render() {
    return (
      <div className={`alert-${this.state.colorType} appNotification`}>
        <span>
          <FontAwesomeIcon icon={this.state.icon} /> &nbsp;
          {this.state.message}
        </span>
      </div>
    );
  }
}

Notification.propTypes = {
  message: PropTypes.string.isRequired,
  colorType: PropTypes.oneOf(["success", "danger", "warning", "info"]),
  icon: PropTypes.any,
  id: PropTypes.number,
};

export default Notification;
