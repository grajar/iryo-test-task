import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { Notification } from "components";
import "./index.scss";

const Notifications = props => (
  <div className="notificationsWrapper">
    {props.notifications.map(notification => {
      return (
        <div key={notification.id}>
          <Notification {...notification} />
        </div>
      );
    })}
  </div>
);

const mapStateToProps = state => {
  return {
    notifications: state.page.notifications,
  };
};

export default compose(connect(mapStateToProps))(Notifications);
