import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
import Panel from "./";

const TabbedPanel = props => {
  const [currentTab, setCurrentTab] = useState(0);

  return Array.isArray(props.tabs) ? (
    <div className="tabbedPanel">
      {props.buttons && (
        <div className="panelButtonsWrapper">
          {props.buttons.map((e, index) => (
            <Fragment key={index}>{e}</Fragment>
          ))}
        </div>
      )}
      {props.tabs.map((tab, index) => (
        <div
          key={index}
          className={`panelTab ${index !== currentTab ? "unselected" : ""}`}
          onClick={() => setCurrentTab(index)}
        >
          {tab.name}
        </div>
      ))}

      <Panel>{props.tabs.length && props.tabs[currentTab].content}</Panel>
    </div>
  ) : null;
};

TabbedPanel.propTypes = {
  tabs: PropTypes.arrayOf(PropTypes.object).isRequired,
  buttons: PropTypes.arrayOf(PropTypes.element),
};

export default TabbedPanel;
