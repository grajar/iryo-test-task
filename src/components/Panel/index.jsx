import React from "react";
import "./index.scss";

const Panel = ({ className, children, ...rest }) => (
  <div {...rest} className={`panel ${className || ""}`}>
    {children}
  </div>
);

export default Panel;
