import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import PropTypes from "prop-types";

import { closePrompt } from "redux/actions";
import { Button, Panel } from "components";
import "./index.scss";
const Prompt = ({ prompt }) => {
  if (prompt.isOpen) {
    return (
      <div className="promptContainer" onClick={() => closePrompt()}>
        <Panel className="promptWindow" onClick={e => e.stopPropagation()}>
          <h2>{prompt.title}</h2>
          <p>{prompt.text}</p>
          <Button
            className="btn-light"
            onClick={() => {
              if (prompt.closeAction) prompt.closeAction();
              closePrompt();
            }}
          >
            {prompt.closeButton || "Close"}
          </Button>
          <Button
            inputProps={{ "data-testid": "prompt-confirm-button" }}
            className="btn-primary"
            onClick={() => {
              prompt.mainAction();
              closePrompt();
            }}
          >
            {prompt.mainButton || "Confirm"}
          </Button>
        </Panel>
      </div>
    );
  }
  return null;
};

Prompt.propTypes = {
  prompt: PropTypes.shape({
    isOpen: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    text: PropTypes.any.isRequired,
    mainAction: PropTypes.func.isRequired,
    mainButton: PropTypes.string,
    closeAction: PropTypes.func,
    closeButton: PropTypes.string,
  }),
};

const mapStateToProps = function(state) {
  return {
    prompt: state.page.prompt,
  };
};

export default compose(connect(mapStateToProps))(Prompt);
