import React from "react";
import PropTypes from "prop-types";

const Select = props => (
  <div className="form-group">
    {props.label && <label>{props.label}</label>}
    <select
      {...props.inputProps}
      type="text"
      className={`form-control ${props.className || ""}`}
      value={props.value}
      onChange={e => props.onChange && props.onChange(e.target.value)}
    >
      {Array.isArray(props.options) &&
        props.options.map(option => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
    </select>
  </div>
);

Select.propTypes = {
  className: PropTypes.string,
  inputProps: PropTypes.object,
  label: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string)).isRequired, // [{label: "123", value: "xyz"}]
  value: PropTypes.string.isRequired,
};

export default Select;
