import React from "react";
import PropTypes from "prop-types";
import "./index.scss";

const Table = props => (
  <div>
    <div className="tableTitleWrapper">
      {props.title && <h2 className="tableTitle">{props.title}</h2>}
      {Array.isArray(props.buttons) && <div className="tableButtonsWrapper">{props.buttons}</div>}
    </div>
    <div className="tableWrapper">
      <table className={`table ${props.className || ""}`}>
        {Array.isArray(props.headers) && (
          <thead className="thead-light">
            <tr>
              {props.headers.map(value => (
                <th key={value}>{value}</th>
              ))}
            </tr>
          </thead>
        )}
        {Array.isArray(props.data) && (
          <tbody>
            {props.data.map(
              (row, index) =>
                Array.isArray(row) &&
                row.length && (
                  <tr key={index}>
                    {row.map((value, index2) => (
                      <td key={index2}>{value}</td>
                    ))}
                  </tr>
                )
            )}
          </tbody>
        )}
      </table>
    </div>
  </div>
);

Table.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  buttons: PropTypes.arrayOf(PropTypes.element),
  headers: PropTypes.arrayOf(PropTypes.string),
  data: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.any)),
};

export default Table;
