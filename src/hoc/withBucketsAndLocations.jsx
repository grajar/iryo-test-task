import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";

import { listBuckets, listLocations } from "api/iryo";
import { FullSizeLoader } from "components";
import fetchStatuses from "constants/fetchStatuses";
import { setStatus } from "redux/actions";
import { showNotification } from "redux/actions";
import { extractErrorMessage } from "utils";

const withBucketsAndLocations = Component => {
  class WithBucketsAndLocations extends React.Component {
    componentDidMount() {
      if (this.props.fetchStatus === fetchStatuses.NOT_FETCHED) {
        this.fetchBuckets();
      }
    }

    fetchBuckets() {
      setStatus(fetchStatuses.FETCHING);
      Promise.all([listBuckets(), listLocations()])
        .then(() => {
          setStatus(fetchStatuses.FETCHED);
        })
        .catch(error => {
          showNotification(extractErrorMessage(error), "danger");
        });
    }

    render() {
      return this.props.fetchStatus === fetchStatuses.FETCHED ? (
        <Component {...this.props} />
      ) : (
        <FullSizeLoader />
      );
    }
  }
  const mapStateToProps = state => {
    return {
      buckets: state.buckets.all,
      locations: state.buckets.locations,
      fetchStatus: state.buckets.status,
    };
  };
  return compose(connect(mapStateToProps))(WithBucketsAndLocations);
};

export default withBucketsAndLocations;
