import React from "react";
import { Provider } from "react-redux";
import store from "redux/store";

import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import App from "./App";
import { Router } from "react-router-dom";

const hist = createBrowserHistory();

ReactDOM.render(
  <Provider store={store}>
    <Router history={hist}>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root")
);
