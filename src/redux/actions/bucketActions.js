import store from "redux/store";
const dispatch = store.dispatch;

export function setBuckets(buckets) {
  dispatch({
    type: "SET_BUCKETS",
    buckets,
  });
}

export function setLocations(locations) {
  dispatch({
    type: "SET_LOCATIONS",
    locations,
  });
}

export function addBucket(bucket) {
  dispatch({
    type: "ADD_BUCKET",
    bucket,
  });
}

export function removeBucket(bucketId) {
  dispatch({
    type: "REMOVE_BUCKET",
    bucketId,
  });
}

export function setObjects(bucketId, objects) {
  dispatch({
    type: "SET_OBJECTS",
    bucketId,
    objects,
  });
}

export function addObject(bucketId, object) {
  dispatch({
    type: "ADD_OBJECT",
    bucketId,
    object,
  });
}

export function removeObject(bucketId, objectName) {
  dispatch({
    type: "REMOVE_OBJECT",
    bucketId,
    objectName,
  });
}

export function setStatus(status) {
  dispatch({
    type: "SET_STATUS",
    status,
  });
}
