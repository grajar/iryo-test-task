import store from "redux/store";
const dispatch = store.dispatch;

let notificationNumber = 0;

export function startLoader() {
  dispatch({
    type: "START_LOADER",
  });
}

export function stopLoader() {
  dispatch({
    type: "STOP_LOADER",
  });
}

export function showNotification(message, colorType, icon) {
  dispatch({
    type: "NOTIFICATION_show",
    message,
    colorType,
    icon,
    id: notificationNumber++,
  });

  setTimeout(() => {
    dispatch({
      type: "NOTIFICATION_close",
    });
  }, 5000);
}

export function closeNotification() {
  dispatch({
    type: "NOTIFICATION_close",
  });
}

export function showPrompt(title, text, mainAction, mainButton, closeAction, closeButton) {
  dispatch({
    type: "SHOW_PROMPT",
    title,
    text,
    mainAction,
    mainButton,
    closeAction,
    closeButton,
  });
}

export function closePrompt() {
  dispatch({
    type: "CLOSE_PROMPT",
  });
}

export function resetStore() {
  dispatch({
    type: "RESET_STORE",
  });
}
