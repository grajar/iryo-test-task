import fetchStatuses from "constants/fetchStatuses";
let initialState = { all: {}, locations: {}, status: fetchStatuses.NOT_FETCHED };

export default function bucketReducer(state, action) {
  if (state === undefined) {
    return initialState;
  }
  var newState = Object.assign({}, state);

  switch (action.type) {
    case "SET_BUCKETS": {
      newState.all = action.buckets;
      break;
    }
    case "ADD_BUCKET": {
      newState.all = Object.assign({}, newState.all);
      newState.all[action.bucket.id] = action.bucket;
      break;
    }
    case "REMOVE_BUCKET": {
      newState.all = Object.assign({}, newState.all);
      delete newState.all[action.bucketId];
      break;
    }
    case "SET_LOCATIONS": {
      newState.locations = action.locations;
      break;
    }
    case "SET_STATUS": {
      newState.status = action.status;
      break;
    }
    case "SET_OBJECTS": {
      newState.all = Object.assign({}, newState.all);
      newState.all[action.bucketId] = Object.assign({}, newState.all[action.bucketId]);

      newState.all[action.bucketId].objects = action.objects;
      break;
    }
    case "ADD_OBJECT": {
      newState.all = Object.assign({}, newState.all);
      newState.all[action.bucketId] = Object.assign({}, newState.all[action.bucketId]);

      if (newState.all[action.bucketId].objects)
        newState.all[action.bucketId].objects = Array.from(newState.all[action.bucketId].objects);
      else newState.all[action.bucketId].objects = [];
      newState.all[action.bucketId].objects.push(action.object);
      break;
    }
    case "REMOVE_OBJECT": {
      newState.all = Object.assign({}, newState.all);
      newState.all[action.bucketId] = Object.assign({}, newState.all[action.bucketId]);

      if (newState.all[action.bucketId].objects && newState.all[action.bucketId].objects.length) {
        newState.all[action.bucketId].objects = Array.from(newState.all[action.bucketId].objects);
        let indexOfObject = newState.all[action.bucketId].objects
          .map(obj => obj.name)
          .indexOf(action.objectName);
        if (indexOfObject >= 0) {
          newState.all[action.bucketId].objects.splice(indexOfObject, 1);
        }
      }
      break;
    }
    case "RESET_STORE":
      newState = initialState;
      break;
    default:
      break;
  }
  return newState;
}
