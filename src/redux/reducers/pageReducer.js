const initialPrompt = {
  isOpen: false,
  title: "",
  text: "",
  mainAction: () => null,
  mainButton: "",
  closeAction: () => null,
  closeButton: "",
};

const initialState = {
  notifications: [],
  prompt: initialPrompt,
};

export default function pageReducer(state, action) {
  if (state === undefined) {
    return initialState;
  }
  var newState = Object.assign({}, state);

  switch (action.type) {
    case "NOTIFICATION_show": {
      newState.notifications = Array.from(newState.notifications);
      newState.notifications.push({
        message: action.message,
        colorType: action.colorType,
        icon: action.icon,
        id: action.id,
      });
      break;
    }

    case "NOTIFICATION_close": {
      newState.notifications = Array.from(newState.notifications);
      newState.notifications.shift();
      break;
    }

    case "SHOW_PROMPT": {
      newState.prompt = Object.assign({}, newState.prompt);
      newState.prompt.isOpen = true;
      newState.prompt.title = action.title;
      newState.prompt.text = action.text;
      newState.prompt.mainAction = action.mainAction;
      newState.prompt.mainButton = action.mainButton;
      newState.prompt.closeAction = action.closeAction;
      newState.prompt.closeButton = action.closeButton;

      break;
    }

    case "CLOSE_PROMPT": {
      newState.prompt = Object.assign({}, newState.prompt);
      newState.prompt.isOpen = false;
      break;
    }

    default:
      break;
  }
  return newState;
}
