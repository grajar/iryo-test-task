import { combineReducers } from "redux";
import bucketReducer from "redux/reducers/bucketReducer";
import pageReducer from "redux/reducers/pageReducer";

const appReducer = combineReducers({
  buckets: bucketReducer,
  page: pageReducer,
});

const rootReducer = (state, action) => {
  if (action.type === "RESET_STORE") state = undefined;
  return appReducer(state, action);
};

export default rootReducer;
