const appRoutes = {
  ROOT: "/",
  BUCKET: "/bucket",
};

export default appRoutes;
