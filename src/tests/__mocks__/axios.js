import urls from "api/iryo/urls";
import { mockGet } from "./iryoMocks";

const mockAxios = jest.genMockFromModule("axios");
// this is the key to fix the axios.create() undefined error!

mockAxios.create = jest.fn(() => mockAxios);
mockAxios.get = jest.fn(url => {
  if (url === urls.BUCKETS) {
    return Promise.resolve({
      data: { buckets: mockGet.buckets },
    });
  } else if (url === urls.LOCATIONS) {
    return Promise.resolve({
      data: { locations: mockGet.locations },
    });
  } else if (url.includes(urls.OBJECTS)) {
    return Promise.resolve({
      data: { objects: mockGet.objects },
    });
  }
});

mockAxios.post = jest.fn((url, data) => {
  if (url.includes(urls.OBJECTS)) {
    return Promise.resolve({
      data: { name: "chucknorris.png", last_modified: "2019-09-18T11:26:51.342Z", size: 2048 },
    });
  } else if (url.includes(urls.BUCKETS)) {
    return Promise.resolve({
      data: { bucket: data },
    });
  }
});

mockAxios.delete = jest.fn(url => {
  if (url.includes(urls.BUCKETS)) {
    return Promise.resolve();
  } else if (url.includes(urls.OBJECTS)) {
    return Promise.resolve();
  }
});

export default mockAxios;
