const mockGet = {
  buckets: [
    {
      id: "first-bucket",
      name: "First Bucket's Name",
      location: { id: "kranstrdam", name: "Kransterdam" },
    },
    {
      id: "second-bucket",
      name: "Second Bucket's Name",
      location: { id: "looblahnah", name: "Looblahnah" },
    },
  ],
  locations: [
    { id: "kranstrdam", name: "Kransterdam" },
    { id: "looblahnah", name: "Looblahnah" },
  ],
  bucket: {
    id: "third",
    name: "third",
    location: { id: "kranstrdam", name: "Kransterdam" },
  },
  objects: [
    {
      name: "first-file.png",
      last_modified: "2019-09-18T09:26:51.342Z",
      size: 1337,
    },
    {
      name: "second-file.png",
      last_modified: "2019-09-18T10:26:51.342Z",
      size: 1911,
    },
  ],
};
const mockPost = {
  bucket: {
    id: "third-bucket",
    name: "Third Bucket's Name",
    location: { id: "looblahnah", name: "Looblahnah" },
  },
};

export { mockGet, mockPost };
