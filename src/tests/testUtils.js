import React from "react";
import { MemoryRouter } from "react-router-dom";

import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import { render, queryByAttribute } from "@testing-library/react";
import { createMemoryHistory } from "history";

import store from "redux/store";
import { resetStore } from "redux/actions";
import { Notifications, Prompt } from "components";

const getById = queryByAttribute.bind(null, "id");
const history = createMemoryHistory();

const BasicProviders = ({ children }) => {
  return (
    <Provider store={store}>
      <Router history={history}>{children}</Router>
    </Provider>
  );
};

const AllProviders = ({ children }) => {
  return (
    <BasicProviders>
      {children}
      <Prompt />
      <Notifications />
    </BasicProviders>
  );
};

const renderAll = (ui, options) => render(ui, { wrapper: AllProviders, ...options });
const renderBasic = (ui, options) => render(ui, { wrapper: BasicProviders, ...options });

export { renderAll as render, renderBasic, resetStore, getById, store, history };
