function extractErrorMessage(error) {
  if (!error) return "An unknown error occured";
  else if (error.response && error.response.data && error.response.data.message)
    return error.response.data.message;
  else if (error.message) return error.message;
  else return error;
}

export default extractErrorMessage;
