const formatDate = date => {
  const newDate = new Date(date);
  return `${newDate.getDate()}.${newDate.getMonth() + 1}.${newDate.getFullYear()}`;
};

const formatFileSize = size => {
  if (!size && size !== 0) return "0B";
  let units = ["B", "KB", "MB", "GB", "TB"];
  let sizeFormatted = size;
  let count = 0;
  while (count < units.length && sizeFormatted > 1023) {
    sizeFormatted = sizeFormatted / 1023;
    count++;
  }
  sizeFormatted = Math.round(sizeFormatted) + units[count];
  return sizeFormatted;
};

export { formatDate, formatFileSize };
