import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Panel, Table, Button } from "components";
import withBucketsAndLocations from "hoc/withBucketsAndLocations";
import CreateBucket from "./CreateBucket";

class BucketList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCreatingNewBucket: false,
      bucketsForTable: [],
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const makeBucketsForTable = buckets => {
      return Object.values(buckets)
        .map(bucket => [
          <Link data-testid="single-bucket-link" to={"/bucket/" + bucket.id}>
            {bucket.name || ""}
          </Link>,
          (bucket.location && bucket.location.name) || "",
        ])
        .sort((a, b) => (a.name > b.name ? 1 : b.name > a.name ? -1 : 0));
    };

    if (
      nextProps.buckets &&
      Object.keys(nextProps.buckets).length !== prevState.bucketsForTable.length
    ) {
      return {
        bucketsForTable: makeBucketsForTable(nextProps.buckets),
      };
    }
    return null;
  }

  onCreateClicked() {
    this.setState({ isCreatingNewBucket: true });
  }
  onBucketCreated() {
    this.setState({ isCreatingNewBucket: false });
  }

  render() {
    return (
      <div data-testid="bucket-list">
        <h1>Bucket list</h1>
        {this.state.isCreatingNewBucket ? (
          <CreateBucket onDone={() => this.onBucketCreated()} />
        ) : null}
        <Panel>
          <Table
            title={`All Buckets (${this.state.bucketsForTable.length})`}
            headers={["Name", "Location"]}
            data={this.state.bucketsForTable}
            buttons={
              !this.state.isCreatingNewBucket
                ? [
                    <Button
                      inputProps={{ "data-testid": "open-create-bucket-button" }}
                      className="btn-primary btn-sm"
                      onClick={() => this.onCreateClicked()}
                      key="create"
                    >
                      Create New Bucket
                    </Button>,
                  ]
                : []
            }
          ></Table>
        </Panel>
      </div>
    );
  }
}

export default withBucketsAndLocations(BucketList);
