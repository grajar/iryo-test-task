import React from "react";
import { cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import api from "axios";
import endpoints from "api/iryo/urls";
import { render, resetStore } from "tests/testUtils";
import { mockGet } from "tests/__mocks__/iryoMocks";
import BucketList from "views/BucketList/BucketList";

jest.mock("axios");

afterEach(() => {
  api.get.mockClear();
  resetStore();
  cleanup();
});

it("renders without crashing", async () => {
  render(<BucketList />);
});

it("calls correct APIs", () => {
  render(<BucketList />);
  expect(api.get).toHaveBeenCalledTimes(2);
  expect(api.get).toHaveBeenCalledWith(endpoints.BUCKETS);
  expect(api.get).toHaveBeenCalledWith(endpoints.LOCATIONS);
});

it("gets past loader", async () => {
  const { findByText, findByTestId, getByTestId } = render(<BucketList />);
  expect(getByTestId("global-loader")).toBeInTheDocument();
  expect(await findByTestId("global-loader")).not.toBeInTheDocument();
  expect(await findByText("Bucket list")).toBeInTheDocument();
});

it("displays fetched data", async () => {
  const { findByText, getAllByRole } = render(<BucketList />);
  expect(await findByText(mockGet.buckets[0].name)).toBeInTheDocument();
  expect(await findByText(mockGet.buckets[0].location.name)).toBeInTheDocument();
  expect(getAllByRole("row").length).toEqual(mockGet.buckets.length + 1); // have 1 header row + as many rows as there are buckets
});

it("opens 'create bucket' panel correctly", async () => {
  const { getByTestId, findByTestId } = render(<BucketList />);
  let openCreateButton = await findByTestId("open-create-bucket-button");
  expect(openCreateButton).toBeInTheDocument();
  fireEvent.click(openCreateButton);
  expect(getByTestId("create-bucket")).toBeInTheDocument();
});

it("display correct links to 'single bucket'", async () => {
  const { getAllByTestId, findByTestId } = render(<BucketList />);
  expect(await findByTestId("global-loader")).not.toBeInTheDocument();
  let singleBucketLinks = getAllByTestId("single-bucket-link");
  expect(singleBucketLinks[0]).toBeInTheDocument();
  expect(singleBucketLinks[0].getAttribute("href")).toEqual("/bucket/" + mockGet.buckets[0].id);
});

// it("matches snapshot", () => {
//   const tree = renderer
//     .create(<BucketFiles bucket={{ id: "1", name: "First Bucket", location: { name: "Kranj" } }} />)
//     .toJSON();
//   expect(tree).toMatchSnapshot();
// });
