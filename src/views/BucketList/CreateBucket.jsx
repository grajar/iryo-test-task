import React, { Component } from "react";

import { Panel, Button, Input, Select } from "components";
import withBucketsAndLocations from "hoc/withBucketsAndLocations";
import { createBucket } from "api/iryo";
import { showNotification } from "redux/actions";
import { extractErrorMessage } from "utils";

class CreateBucket extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      location: "",
      locationOptions: [],
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (Object.keys(nextProps.locations).length !== prevState.locationOptions.length) {
      const makeLocationOptions = locations => {
        return Object.values(locations).map(location => ({
          label: location.name,
          value: location.id,
        }));
      };
      const locationOptions = makeLocationOptions(nextProps.locations);
      return {
        locationOptions: makeLocationOptions(nextProps.locations),
        location: locationOptions[0] ? locationOptions[0].value : "",
      };
    }
    return null;
  }

  onCreateBucketClicked() {
    if (!this.state.name) {
      showNotification("Please enter a new bucket name", "warning");
    } else {
      createBucket(this.state.name, this.state.location)
        .then(bucket => {
          showNotification("Bucket successfully created", "success");
          this.props.onDone && this.props.onDone();
        })
        .catch(error => {
          showNotification(extractErrorMessage(error), "danger");
        });
    }
  }

  render() {
    return (
      <div data-testid="create-bucket">
        <h2>Create new bucket</h2>
        <Panel>
          <form>
            <div className="row">
              <div className="col">
                <Input
                  value={this.state.name}
                  onChange={name => {
                    this.setState({ name });
                  }}
                  label="Bucket Name*"
                  placeholder="Enter a new name"
                />
              </div>
              <div className="col">
                <Select
                  label="Bucket Location*"
                  options={this.state.locationOptions}
                  onChange={location => {
                    this.setState({ location });
                  }}
                  value={this.state.location}
                />
              </div>
            </div>
            <Button
              inputProps={{ "data-testid": "create-bucket-submit-button" }}
              className="btn-primary"
              onClick={e => {
                e.preventDefault();
                this.onCreateBucketClicked();
              }}
            >
              Create Bucket
            </Button>
          </form>
        </Panel>
      </div>
    );
  }
}

export default withBucketsAndLocations(CreateBucket);
