import React from "react";
import { cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import api from "axios";
import endpoints from "api/iryo/urls";
import { render, resetStore } from "tests/testUtils";
import { mockGet, mockPost } from "tests/__mocks__/iryoMocks";
import CreateBucket from "views/BucketList/CreateBucket";

jest.mock("axios");

afterEach(() => {
  api.get.mockClear();
  api.post.mockClear();
  resetStore();
  cleanup();
});

it("renders without crashing", async () => {
  render(<CreateBucket />);
});

it("calls correct APIs", () => {
  render(<CreateBucket />);
  expect(api.get).toHaveBeenCalledTimes(2);
  expect(api.get).toHaveBeenCalledWith(endpoints.BUCKETS);
  expect(api.get).toHaveBeenCalledWith(endpoints.LOCATIONS);
});

it("gets past loader", async () => {
  const { findByTestId, getByTestId } = render(<CreateBucket />);
  expect(getByTestId("global-loader")).toBeInTheDocument();
  expect(await findByTestId("global-loader")).not.toBeInTheDocument();
});

it("displays form correctly", async () => {
  const { getByRole, getByText, findByTestId, getByTestId } = render(<CreateBucket />);
  expect(await findByTestId("global-loader")).not.toBeInTheDocument();
  expect(getByRole("textbox")).toBeInTheDocument(); // name input
  expect(getByRole("listbox")).toBeInTheDocument(); // location select
  expect(getByRole("button")).toBeInTheDocument(); // submit button

  mockGet.locations.forEach(location => {
    expect(getByText(location.name)).toBeInTheDocument();
  });
});

it("creates a new bucket", async () => {
  const { getByRole, findByTestId, getByTestId } = render(<CreateBucket />);
  expect(await findByTestId("global-loader")).not.toBeInTheDocument();

  // enter new bucket name
  const nameInput = getByRole("textbox");
  fireEvent.change(nameInput, { target: { value: mockPost.bucket.name } });
  expect(nameInput.value).toEqual(mockPost.bucket.name);

  // select location
  const locationInput = getByRole("listbox");
  fireEvent.change(locationInput, {
    target: { value: mockPost.bucket.location.id, label: mockPost.bucket.location.name },
  });
  expect(locationInput.value).toEqual(mockPost.bucket.location.id);

  // submit new bucket
  const submitButton = getByTestId("create-bucket-submit-button"); // submit button
  expect(submitButton).toBeInTheDocument();
  expect(api.post).toHaveBeenCalledTimes(0);
  fireEvent.click(submitButton);

  // check if created
  expect(api.post).toHaveBeenCalledTimes(1);
  expect(api.post).toHaveBeenCalledWith(endpoints.BUCKETS, {
    name: mockPost.bucket.name,
    location: mockPost.bucket.location.id,
  });
});
