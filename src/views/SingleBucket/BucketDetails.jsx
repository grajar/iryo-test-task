import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { Table } from "components";
import "./index.scss";
import { formatFileSize } from "utils";

const BucketDetails = ({ bucket }) => {
  let tableData = bucket
    ? [
        ["Bucket name:", bucket.name || ""],
        ["Location:", bucket.location ? bucket.location.name : ""],
        [
          "Storage size:",
          bucket.objects && bucket.objects.length
            ? formatFileSize(bucket.objects.reduce((acc, obj) => acc + (obj.size || 0), 0))
            : "0B",
        ],
      ]
    : [[]];
  return (
    <Fragment>
      <Table className="bucketDetailsTable" data={tableData} />
    </Fragment>
  );
};

BucketDetails.propTypes = {
  bucket: PropTypes.object.isRequired,
};

export default BucketDetails;
