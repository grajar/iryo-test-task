import React from "react";
import { cleanup } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import { render, resetStore } from "tests/testUtils";
import { mockPost } from "tests/__mocks__/iryoMocks";
import BucketDetails from "views/SingleBucket/BucketDetails";

afterEach(() => {
  resetStore();
  cleanup();
});

it("renders without crashing", () => {
  render(<BucketDetails bucket={mockPost.bucket} />);
});

it("displays bucket details", () => {
  const { getByText } = render(<BucketDetails bucket={mockPost.bucket} />);
  expect(getByText(mockPost.bucket.name)).toBeInTheDocument();
  expect(getByText(mockPost.bucket.location.name)).toBeInTheDocument();
});
