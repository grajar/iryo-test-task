import React, { Fragment, useRef, useState } from "react";
import PropTypes from "prop-types";

import { createObject, deleteObject } from "api/iryo";
import { Table, Button } from "components";
import "./index.scss";
import { extractErrorMessage, formatDate, formatFileSize } from "utils";
import { showNotification, showPrompt } from "redux/actions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFile } from "@fortawesome/free-solid-svg-icons";

const BucketFiles = ({ bucket }) => {
  const inputFile = useRef(null);
  const [selectedFiles, setSelectedFiles] = useState(
    Array.isArray(bucket.objects) ? new Array(bucket.objects.length) : []
  );

  const uploadFile = async file => {
    let object = await createObject(bucket.id, file).catch(error =>
      showNotification(extractErrorMessage(error), "danger")
    );
    if (object) showNotification(`Successfully uploaded ${object.name}`, "success");
    inputFile.current.value = null;
  };

  const onDeleteClicked = () => {
    const deleteObjects = async () => {
      let filesToDelete = bucket.objects.filter((file, index) => !!selectedFiles[index]);
      let deletePromises = filesToDelete.map(file => {
        return deleteObject(bucket.id, file.name);
      });
      return await Promise.all(deletePromises)
        .then(() =>
          showNotification(`Successfully deleted ${deletePromises.length} files`, "success")
        )
        .catch(err => showNotification(extractErrorMessage(err), "danger"));
    };
    const fileLength = selectedFiles.reduce((acc, cur) => acc + (cur ? 1 : 0), 0);
    showPrompt(
      "Are you sure?",
      `${fileLength} files will be permanently deleted.`,
      () => deleteObjects(),
      "Delete"
    );
  };

  const objectsForTable = bucket.objects
    ? bucket.objects.map((object, index) => {
        return [
          <input
            checked={!!selectedFiles[index]}
            type="checkbox"
            onChange={e => {
              let newSelectedFiles = Array.from(selectedFiles);
              newSelectedFiles[index] = !selectedFiles[index];
              setSelectedFiles(Array.from(newSelectedFiles));
            }}
          />,
          <span>
            <FontAwesomeIcon icon={faFile} />
            &nbsp;
            {object.name}
          </span>,
          formatDate(object.last_modified),
          formatFileSize(object.size),
        ];
      })
    : [];

  return (
    <Fragment>
      <input
        data-testid="upload-file-input"
        className="bucketFilesInput"
        onChange={event => uploadFile(event.target.files[0])}
        ref={inputFile}
        type="file"
      />
      <Table
        headers={["", "Name", "Last Modified", "Size"]}
        buttons={[
          <Button
            onClick={() => onDeleteClicked()}
            className="btn-sm btn-outline-danger"
            key="delete"
          >
            Delete Objects
          </Button>,
          <Button
            onClick={() => {
              inputFile.current.click();
            }}
            className="btn-sm btn-outline-primary"
            key="upload"
          >
            Upload Object
          </Button>,
        ]}
        data={objectsForTable || []}
      />
    </Fragment>
  );
};

BucketFiles.propTypes = {
  bucket: PropTypes.object,
};

export default BucketFiles;
