import React from "react";
import { cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import api from "axios";
import endpoints from "api/iryo/urls";
import { render, resetStore } from "tests/testUtils";
import { mockGet, mockPost } from "tests/__mocks__/iryoMocks";
import BucketFiles from "views/SingleBucket/BucketFiles";

jest.mock("axios");

afterEach(() => {
  api.get.mockClear();
  api.post.mockClear();
  api.delete.mockClear();
  resetStore();
  cleanup();
});

it("renders without crashing", () => {
  render(<BucketFiles bucket={mockPost.bucket} />);
});

it("displays bucket objects", () => {
  const { getByText } = render(
    <BucketFiles bucket={{ ...mockGet.bucket, objects: mockGet.objects }} />
  );
  expect(getByText(mockGet.objects[0].name)).toBeInTheDocument();
});

it("deletes an object", async () => {
  const { getAllByRole, getByText, findByTestId } = render(
    <BucketFiles bucket={{ ...mockGet.bucket, objects: mockGet.objects }} />
  );

  // select first file
  expect(getAllByRole("checkbox").length).toEqual(mockGet.objects.length);
  fireEvent.click(getAllByRole("checkbox")[0]);
  expect(getAllByRole("checkbox").filter(is => is.checked).length).toEqual(1);

  // click delete objects
  const deleteObjectsButton = getByText("Delete objects", { exact: false });
  expect(deleteObjectsButton).toBeInTheDocument();
  fireEvent.click(deleteObjectsButton);

  // confirm delete in prompt
  const confirmDeleteButton = await findByTestId("prompt-confirm-button");
  expect(confirmDeleteButton).toBeInTheDocument();
  fireEvent.click(confirmDeleteButton);

  // check that delete is called correctly
  expect(api.delete).toBeCalledTimes(1);
  expect(api.delete).toBeCalledWith(
    `${endpoints.BUCKETS}/${mockGet.bucket.id}${endpoints.OBJECTS}/${mockGet.objects[0].name}`
  );
});

it("uploads an object", async () => {
  const { getByText, getByTestId } = render(
    <BucketFiles bucket={{ ...mockGet.bucket, objects: mockGet.objects }} />
  );

  // click upload objects
  const uploadObjectsButton = getByText("Upload Object", { exact: false });
  expect(uploadObjectsButton).toBeInTheDocument();
  fireEvent.click(uploadObjectsButton);

  // fake adding file to input
  const uploadInput = getByTestId("upload-file-input");
  const file = new File(["(⌐□_□)"], "chucknorris.png", { type: "image/png" });

  fireEvent.change(uploadInput, {
    target: {
      files: [file],
    },
  });

  // check if file uploads
  expect(api.post).toBeCalledTimes(1);
  expect(api.post).toBeCalledWith(
    `${endpoints.BUCKETS}/${mockGet.bucket.id}${endpoints.OBJECTS}`,
    expect.anything()
  );
});
