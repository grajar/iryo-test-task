import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import withBucketsAndLocations from "hoc/withBucketsAndLocations";
import { TabbedPanel, Button } from "components";
import { listObjects, deleteBucket } from "api/iryo";
import NotFound from "views/NotFound";
import { extractErrorMessage } from "utils";
import { showNotification } from "redux/actions";
import BucketFiles from "./BucketFiles";
import BucketDetails from "./BucketDetails";
import appRoutes from "routes";

class SingleBucket extends Component {
  componentDidMount() {
    listObjects(this.props.match.params.bucketId || "").catch(error =>
      showNotification(extractErrorMessage(error))
    );
  }

  onDeleteClicked() {
    deleteBucket(this.props.match.params.bucketId)
      .then(() => {
        showNotification("Bucket successfully deleted", "success");
        this.props.history.push(appRoutes.ROOT);
      })
      .catch(error => showNotification(extractErrorMessage(error)));
  }

  renderFiles() {
    return <BucketFiles bucket={this.props.buckets[this.props.match.params.bucketId || ""]} />;
  }
  renderDetails() {
    return <BucketDetails bucket={this.props.buckets[this.props.match.params.bucketId || ""]} />;
  }

  render() {
    if (!this.props.buckets[this.props.match.params.bucketId || ""]) return <NotFound />;
    return (
      <div data-testid="single-bucket">
        <h1>Bucket {this.props.buckets[this.props.match.params.bucketId || ""].name}</h1>
        <TabbedPanel
          buttons={[
            <Button
              inputProps={{ "data-testid": "delete-bucket-button" }}
              onClick={() => this.onDeleteClicked()}
              className="btn-danger"
            >
              Delete Bucket
            </Button>,
          ]}
          tabs={[
            { name: "Files", content: this.renderFiles() },
            { name: "Details", content: this.renderDetails() },
          ]}
        />
      </div>
    );
  }
}

export default withRouter(withBucketsAndLocations(SingleBucket));
