import React from "react";
import { Switch, Route } from "react-router-dom";
import { cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import api from "axios";
import endpoints from "api/iryo/urls";
import { render, resetStore, history } from "tests/testUtils";
import { mockGet } from "tests/__mocks__/iryoMocks";
import SingleBucket from "views/SingleBucket/SingleBucket";
import appRoutes from "routes";

jest.mock("axios");

afterEach(() => {
  history.push("/");
  api.get.mockClear();
  api.post.mockClear();
  api.delete.mockClear();
  resetStore();
  cleanup();
});

it("renders without crashing", async () => {
  render(<SingleBucket />);
});

it("gets past loader", async () => {
  history.push(`${appRoutes.BUCKET}/${mockGet.buckets[0].id}`);

  const { findByTestId, getByTestId } = render(
    <Switch>
      <Route path={appRoutes.BUCKET + "/:bucketId?"}>
        <SingleBucket />
      </Route>
    </Switch>
  );

  expect(getByTestId("global-loader")).toBeInTheDocument();
  expect(await findByTestId("global-loader")).not.toBeInTheDocument();
});

it("calls correct APIs", async () => {
  history.push(`${appRoutes.BUCKET}/${mockGet.bucket.id}`);

  const { findByTestId, getByTestId } = render(
    <Switch>
      <Route path={`${appRoutes.BUCKET}/:bucketId?`}>
        <SingleBucket />
      </Route>
    </Switch>
  );
  expect(getByTestId("global-loader")).toBeInTheDocument();
  expect(await findByTestId("global-loader")).not.toBeInTheDocument();

  expect(api.get).toHaveBeenCalledTimes(3);
  expect(api.get).toHaveBeenCalledWith(endpoints.BUCKETS);
  expect(api.get).toHaveBeenCalledWith(endpoints.LOCATIONS);
  expect(api.get).toHaveBeenCalledWith(
    `${endpoints.BUCKETS}/${mockGet.bucket.id}${endpoints.OBJECTS}`
  );
});

it("displays fetched data", async () => {
  history.push(`${appRoutes.BUCKET}/${mockGet.buckets[0].id}`);
  const { findByText, findByTestId } = render(
    <Switch>
      <Route path={`${appRoutes.BUCKET}/:bucketId?`}>
        <SingleBucket />
      </Route>
    </Switch>
  );
  expect(await findByTestId("single-bucket")).toBeInTheDocument();
  expect(await findByText(mockGet.buckets[0].name, { exact: false })).toBeInTheDocument();
  expect(await findByText(mockGet.objects[0].name, { exact: false })).toBeInTheDocument();
});

it("uploads and displays an object", async () => {
  history.push(`${appRoutes.BUCKET}/${mockGet.buckets[0].id}`);
  const { getByText, getByTestId, findByTestId, findByText } = render(
    <Switch>
      <Route path={`${appRoutes.BUCKET}/:bucketId?`}>
        <SingleBucket />
      </Route>
    </Switch>
  );
  expect(await findByTestId("single-bucket")).toBeInTheDocument();

  // click upload objects
  const uploadObjectsButton = await getByText("Upload Object", { exact: false });
  expect(uploadObjectsButton).toBeInTheDocument();
  fireEvent.click(uploadObjectsButton);

  // fake adding file to input
  const uploadInput = getByTestId("upload-file-input");
  const file = new File(["(⌐□_□)"], "chucknorris.png", { type: "image/png" });

  fireEvent.change(uploadInput, {
    target: {
      files: [file],
    },
  });

  // check if file uploads and displays
  expect(api.post).toBeCalledTimes(1);
  expect(api.post).toBeCalledWith(
    `${endpoints.BUCKETS}/${mockGet.buckets[0].id}${endpoints.OBJECTS}`,
    expect.anything()
  );
});

it("switches between tabs", async () => {
  history.push(`${appRoutes.BUCKET}/${mockGet.buckets[0].id}`);
  const { getByText, findByTestId, findByText } = render(
    <Switch>
      <Route path={`${appRoutes.BUCKET}/:bucketId?`}>
        <SingleBucket />
      </Route>
    </Switch>
  );
  expect(await findByTestId("single-bucket")).toBeInTheDocument();

  // Switch to 'Details' tab
  const detailsTab = await findByText("Details");
  expect(detailsTab).toBeInTheDocument();
  fireEvent.click(detailsTab);
  expect(await findByText("bucket name", { exact: false })).toBeInTheDocument();
  expect(getByText("location", { exact: false })).toBeInTheDocument();
  expect(getByText("size", { exact: false })).toBeInTheDocument();

  // Switch to 'Files' tab
  const filesTab = getByText("Files");
  expect(filesTab).toBeInTheDocument();
  fireEvent.click(filesTab);
  expect(await findByTestId("upload-file-input")).toBeInTheDocument();
});

it("deletes current bucket", async () => {
  history.push(`${appRoutes.BUCKET}/${mockGet.buckets[0].id}`);
  const { getAllByTestId, findByTestId, findByText } = render(
    <Switch>
      <Route path={`${appRoutes.BUCKET}/:bucketId?`}>
        <SingleBucket />
      </Route>
    </Switch>
  );
  expect(await findByTestId("single-bucket")).toBeInTheDocument();

  const deleteBucketButton = await findByTestId("delete-bucket-button");
  expect(deleteBucketButton).toBeInTheDocument();
  fireEvent.click(deleteBucketButton);
  expect(history.entries[0].pathname).toEqual(appRoutes.ROOT);
});

// it("matches snapshot", () => {
//   const tree = renderer
//     .create(<BucketFiles bucket={{ id: "1", name: "First Bucket", location: { name: "Kranj" } }} />)
//     .toJSON();
//   expect(tree).toMatchSnapshot();
// });
